import sys
# import PIL.Image
# from PIL import Image
import cv2
import numpy as np
from skimage.measure import compare_ssim
from skimage.transform import resize
from warnings import filterwarnings

filterwarnings("ignore")           # To ignore deprecation warning

image1 = cv2.imread(sys.argv[1])
image2 = cv2.imread(sys.argv[2])

"""
To check whether color or black and white
"""
def check_gray(img):
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            b, g, r = img[x,y]
            if not(b == g == r):
                print("color")
                return False,"color"
            if ((b == g == r ==255) or (b == g == r ==0)):
                print("black and white")
                return False,"black_white"
            if not ((b == g == r ==255) or (b == g == r ==0)):
                print("grey")
                return False,"grey"
            
    print("black and white")
    return True,"black_white" 

res1,color = check_gray(image1)
print(res1,color)
# if res1 == True:
if color == "grey":
    print("Image 1 is Grey")
elif color == "black_white":
    print("Image 1 is Black and white ")
else:
    print("Image 1 is Color")

res2,color2 = check_gray(image2)
if color2 == "grey":
    print("Image 2 is Grey")
elif color2 == "black_white":
    print("Image 2 is Black and white ")
else:
    print("Image 2 is Color")


"""
To get the image difference in percetage
"""
img_a = resize(image1, (2**10, 2**10))
img_b = resize(image2, (2**10, 2**10))

# score: {-1:1} measure of the structural similarity between the images
score, diff = compare_ssim(img_a, img_b, full=True,multichannel=True)
print("Imgae differnce is : ",(1-score)*100, "%")


